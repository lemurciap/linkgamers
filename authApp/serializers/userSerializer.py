from rest_framework import serializers
from authApp.models.user import User
from authApp.models.game import Game
from rest_framework.relations import PrimaryKeyRelatedField


class UserSerializer(serializers.ModelSerializer):
    games = PrimaryKeyRelatedField(queryset= Game.objects.all(), many=True)
    
    class Meta:
        model = User
        fields = [ 'username', 'password', 'email', 'games', 'linkFoto', 'presentacion']
    
    def create(self, validated_data):
        games = validated_data.pop('games')
        userInstance = User.objects.create(**validated_data)
        userInstance.games.set(games)
        return userInstance
    
    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        games=[]
        for game in user.games.all():
            games.append(game.codigo_juego)
        return {
            'id': user.id,
            'username': user.username,
            'email': user.email,
            'linkFoto': user.linkFoto,
            'presentacion': user.presentacion,          
            'games': games,
            #'friends': user.friends,
        

          
        }   