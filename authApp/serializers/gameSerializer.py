from authApp.models.game import Game
from rest_framework import serializers
class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ['nombre_juego', 'tipo_juego']