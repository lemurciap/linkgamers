# Generated by Django 3.2.8 on 2021-10-15 17:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authApp', '0002_auto_20211007_2231'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='friends',
        ),
        migrations.AddField(
            model_name='user',
            name='linkFoto',
            field=models.CharField(default='https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_640.png', max_length=300, verbose_name='LinkFoto'),
        ),
        migrations.AddField(
            model_name='user',
            name='presentacion',
            field=models.CharField(default='Hola, soy un nuevo jugador con ganas de conocer compañeros de juegos', max_length=200, verbose_name='Presentacion'),
        ),
        migrations.DeleteModel(
            name='Perfil',
        ),
    ]
